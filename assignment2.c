//Enter your code here
//2a.c
#include<stdio.h>
void main()
{
printf("Swaroop Jadhav\n");
}

//2b.c
#include<stdio.h>
void main()
{
int a=10;
int b=20;
int sum=a+b;
printf("The sum of the numbers is=%d\n",sum);
}

//2c.c
#include<stdio.h>
void main()
{
int r=10;
float area=3.1415*r*r;
printf("The area of the circle=%f\n",area);
}

//2d.c
#include<stdio.h>
void main()
{
float area,r;
printf("Enter the radius of the circle\n");
scanf("%f",&r);
area=3.1415*r*r;
printf("Area of the circle=%f\n",area);
}

//2e.c
#include<stdio.h>
void main()
{
int x,y;
x=10;
y=20;
printf("\n %d<%d=%d",x,y,x<y);
printf("\n %d>%d=%d",x,y,x>y);
printf("\n %d<=%d=%d",x,y,x<=y);
printf("\n %d>=%d=%d",x,y,x>=y);
printf("\n %d==%d=%d",x,y,x==y);
printf("\n %d!=%d=%d",x,y,x!=y);
}

//2f.c
#include<stdio.h>
void main()
{
float p,t,r,si;
printf("Enter the Principle Amount,Time and Rate of interest\n");
scanf("%f %f %f",&p,&t,&r);
si=(p*t*r)/100;
printf("The Simple Interest is %f\n",si);
}