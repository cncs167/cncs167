//write your code here
#include <stdio.h>
void operation(int *a,int *b);
int main()
{
    int n1,n2;  
    printf("Enter the first number: \n");
    scanf("%d",&n1);
    printf("Enter the second number: \n");
    scanf("%d",&n2);
    operation(&n1,&n2);
    return 0;
}

void operation(int *a,int *b)
{
    printf("The sum of the two numbers %d and %d is %d\n",*a,*b,(*a+*b));
    printf("The difference of the two numbers %d and %d is %d\n",*a,*b,(*a-*b));
    printf("The product of the two numbers %d and %d is %d\n",*a,*b,((*a)*(*b)));
    printf("The quotient of the two numbers %d and %d is %d\n",*a,*b,((*a)/(*b)));
    printf("The remainder of the two numbers %d and %d is %d\n",*a,*b,(*a%*b));
}