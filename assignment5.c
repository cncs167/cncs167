//Enter your code here
//5a.c

#include<stdio.h>
int main()
{
int i=1,sum=0;
while(i<=10)
{
sum=sum+i;
i++;
}
printf("The sum of first 10 natural numbers is = %d",sum);
return 0;
}

//5b.c
#include<stdio.h>
int main()
{
for(int i=1;i<=20;i++)
printf("%d\t",i)
return 0;
}

//5c.c
#include<stdio.h>
int main()
{
int n,temp;
printf("Enter the three digit number\n");
scanf("%d",&n);
while(n!=0)
{
temp= n%10;
printf("%d",temp);
n=n/10;
}
return 0;
}

//5d.c
#include<stdio.h>
int main()
{
int i,t;
for(i=1;i<=10;i++)
{
t=12*i;
printf("12*%d=%d\n",i,t);
}
return 0;
}