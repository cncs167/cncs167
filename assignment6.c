//Enter your code here
//6a.c
#include <stdio.h>
int main()
{
    int i,n,fact=1;
    printf("Enter the number\n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        fact=fact*i;
    }
    printf("The factorial of the given number is=%d",fact);
    return 0;
}

//6b.c
#include <stdio.h>
int main()
{
    int n,q,r;
    printf("Enter the number\n");
    scanf("%d",&n);
     q=n;
    while(q!=0)
    {
        r=q%10;
        q=q/10;
        printf("%d",r);
    }
    return 0;
}


//6c.c
#include <stdio.h>
int main()
{
    int i,n,j;
    float rec,s=0;
    printf("Enter the number of terms\n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        j=i*i;
        rec=1/(float)j;
        s=s+rec;
        
    }
    printf("The sum of the terms is=%f",s);
    return 0;   
}

//6d.c
#include <stdio.h>
int main()
{
    int n,i,j;
    printf("Enter the number of terms\n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        printf("\n");
        for(j=1;j<=i;j++)
        {
            printf("%d",i);
        }
    }
    return 0;
}

//6e.c
#include <stdio.h>
int main()
{
    char n,i,j;
    printf("Enter the last alphabet\n");
    scanf("%c",&n);
    for(i='A';i<=n;i++)
    {
        printf("\n");
        for(j='A';j<=i;j++)
        {
            printf("%c",j);
        }
    }
    return 0;
}

//6f.c
#include <stdio.h>
int main()
{
    int n1, n2, i, gcd;

    printf("Enter two integers: ");
    scanf("%d %d", &n1, &n2);

    for(i=1; i <= n1 && i <= n2; ++i)
    {
        if(n1%i==0 && n2%i==0)
            gcd = i;
    }

    printf("G.C.D of %d and %d is %d", n1, n2, gcd);

    return 0;
}