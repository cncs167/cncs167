//write your code here
#include <stdio.h>

int main()
{
    char a;
    char *b;
    b=&a;
    printf("Enter the character\n");
    scanf("%c",&a);
    if(*b=='a'||*b=='e'||*b=='i'||*b=='o'||*b=='u')
        printf("The entered character is vowel\n");
    else if(*b=='A'||*b=='E'||*b=='I'||*b=='O'||*b=='U')
        printf("The entered character is vowel\n");
    else
        printf("The entered character is consonant\n");
    return 0;
}

//pointers for swapping two numbers
#include <stdio.h>
 
int main()
{
   int x, y, *a, *b, temp;
 
   printf("Enter the value of x and y\n");
   scanf("%d%d", &x, &y);
 
   printf("Before Swapping\nx = %d\ny = %d\n", x, y);
 
   a = &x;
   b = &y;
 
   temp = *b;
   *b = *a;
   *a = temp;
 
   printf("After Swapping\nx = %d\ny = %d\n", x, y);
 
   return 0;
}