//write your code here
#include<stdio.h>
int factorial(int);
void main()
{
    int i,n,j,f=1;
    float s=0,fac;
    printf("Enter the number of terms\n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        f=factorial(i);
        fac=1/(float)f;
        s=s+fac;
    }
    printf("The sum of the series is=%f\n",s);
}

int factorial(int i)
{
    int f=1,j;
    for(j=1;j<=i;j++)
    {
        f=f*j;
    }
    return f;
}
