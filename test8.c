//write your code here
#include <stdio.h>
struct student 
{
    char Name[100];
    int roll;
    float fees;                    
    char department[100];
    char section;               
    int marks;                   
} s[100];

int main() 
{
    int i,n,highest=0,j;
    printf("Enter the  number of students:\n");
    scanf("%d",&n);
    printf("Enter information of students:\n");

    for (i = 0; i < n; ++i) 
    {
        s[i].roll = i + 1;
        printf("\nFor roll number %d,\n", s[i].roll);
        printf("Enter  name: ");
        scanf("%s", s[i].Name);
        printf("Enter the section: ");
        scanf("%s",&s[i].section);      
        printf("Enter fees: ");
        scanf("%f", &s[i].fees);         
        printf("Enter the Department:\n");
        scanf("%s",s[i].department);
        printf("Enter the marks : ");  
        scanf("%d",&s[i].marks);            
    }
    
    for(i=0;i<n;i++)
    {
        if(s[i].marks>highest)                  
        {
            highest=s[i].marks;
            j=i;
        }
        
        
    }
    printf("Details of the student scoring the highest marks :\n");    
    printf("\nRoll number: %d\n", j + 1);
    printf("First name: ");
    puts(s[j].Name);
    printf("Section : %c\n",s[j].section);       
    printf("Fees: %.1f\n", s[j].fees);
    printf("Department: ");
    printf("%s\n",s[j].department);                          
    printf("Marks= %d",s[j].marks);
    printf("\n");
    
    return 0;
}

