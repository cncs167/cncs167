//write your code here
#include <stdio.h>
int main()
{
    int arr[10],i,n,pos=-1,flag,beg,end,mid,key;
    printf("Enter the Number of elements\n");
    scanf("%d",&n);
    printf(" Enter the Elements:\n");
    {
        for(i=0;i<n;i++)
        scanf("%d",&arr[i]);
    }
    printf("Enter the key element to be searched\n");
    scanf("%d",&key);
    int low=0;
    int high=n-1;
    while(low<high)
    {  
        mid=(low+high)/2;
        if (arr[mid]==key)
          {
            printf("\n %d is present in the array at position = %d",key,mid);
            flag=1;
            break;
          }
        else if(arr[mid]>key)
        high=mid-1;
        else
        low=mid+1;
    }
    if (low>high  && flag==0)
    printf("\n %d does not exist in the array",key);
     return 0;
}