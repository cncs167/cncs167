//write your code here
#include <stdio.h>
struct student {
    char Name[50];
    int roll;
    float fees;
    int DOB[100];
} s[100];

int main() {
    int i,n;
    printf("Enter the  number of students:\n");
    scanf("%d",&n);
    printf("Enter information of students:\n");

    for (i = 0; i < n; ++i) 
    {
        s[i].roll = i + 1;
        printf("\nFor roll number%d,\n", s[i].roll);
        printf("Enter first name: ");
        scanf("%s", s[i].Name);
        printf("Enter fees: ");
        scanf("%f", &s[i].fees);
        printf("Enter DOB:\n");
        scanf("%s",s[i].DOB);
    }
    printf("Displaying Information:\n\n");

    for (i = 0; i < n; ++i) {
        printf("\nRoll number: %d\n", i + 1);
        printf("First name: ");
        puts(s[i].Name);
        printf("Fees: %.1f\n", s[i].fees);
        printf("DOB: ");
        puts(s[i].DOB);
        printf("\n");
    }
    return 0;
}