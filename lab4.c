//write your code here
#include <stdio.h>
int main()
{
    int a[50],n,i=1,pos,ele;
    printf("Enter the number of terms in the array\n");
    scanf("%d",&n);
    printf("Enter the array terms\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the position of the number to be inserted\n");
    scanf("%d",&pos);
    for(i=n;i>pos-1;i--)
    {
        a[i]=a[i-1];
    }
    printf("Enter the element to be added\n");
    scanf("%d",&ele);
    a[pos]=ele;
    n=n+1;
    printf("Array after insertion\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    return 0;
}