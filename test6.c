//write your code here
#include <stdio.h>
int main() {
    int a[10][10], transpose[10][10], i, j;
    

    printf("\nEnter matrix elements:\n");
    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j) {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &a[i][j]);
        }

    printf("\nEntered matrix: \n");
    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j) {
            printf("%d  ", a[i][j]);
            if (j == 3 - 1)
                printf("\n");
        }

    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j) {
            transpose[j][i] = a[i][j];
        }

    printf("\nTranspose of the matrix:\n");
    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j) {
            printf("%d  ", transpose[i][j]);
            if (j == 3 - 1)
                printf("\n");
        }
    return 0;
}